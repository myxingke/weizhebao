$(function(){
        var scale = 1 / devicePixelRatio;
    //document.querySelector('meta[name="viewport"]').setAttribute('content', 'initial-scale=' + scale + ', maximum-scale=' + scale + ', minimum-scale=' + scale + ', user-scalable=no');
    $(window).resize(function(){
        //var deviceWidth = document.documentElement.clientWidth > 1300 ? 1300 : document.documentElement.clientWidth;
        //document.documentElement.style.fontSize = (deviceWidth / 6.4) + 'px';
        var deviceWidth = document.documentElement.clientWidth;
        if(deviceWidth > 640) deviceWidth = 640;
        document.documentElement.style.fontSize = deviceWidth / 6.4 + 'px';
    }).resize();

});

//// 通过js适配不同的屏幕大小
//(function (doc, win) {
//    var docEl = doc.documentElement,
//        resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
//        recalc = function () {
//            var clientWidth = docEl.clientWidth;
//            if (!clientWidth) return;
//            // 根据设备的比例调整初始font-size大小
//            if(clientWidth>640) clientWidth = 640;
//            docEl.style.fontSize = 50 * (clientWidth / 320) + 'px';
//        };
//    if (!doc.addEventListener) return;
//    win.addEventListener(resizeEvt, recalc, false);
//    doc.addEventListener('DOMContentLoaded', recalc, false);
//})(document, window);